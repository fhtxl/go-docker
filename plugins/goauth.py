from godocker.iAuthPlugin import IAuthPlugin
import grp
import os

from ldap3 import Server, Connection, SUBTREE


class GoAuth(IAuthPlugin):
    def get_name(self):
        return "goauth"

    def get_type(self):
        return "Auth"

    def get_groups(self, user_id):
        gids = [g.gr_gid for g in grp.getgrall() if user_id in g.gr_mem]
        return [grp.getgrgid(gid).gr_gid for gid in gids]

    def __is_config_param_defined(self, param):
        if param not in self.cfg:
            return False
        # If empty string, empty array or None
        if not self.cfg[param]:
            return False
        return True

    def __get_ldap_groups(self, con, userId):
        '''
        Get secondary groups for userId from LDAP.

        If no result, try to get them from localhost.

        :param con: LDAP connnection object
        :type con: ldap3 connection object
        :param userId: User identifier
        :type userId: str
        :return: dict with user information
        '''
        sgids = []
        try:
            ldap_dn = self.cfg['ldap_dn']
            ldap_base_dn_group_filter = 'ou=Groups'
            if self.__is_config_param_defined('ldap_base_dn_group_filter'):
                ldap_base_dn_group_filter = self.cfg['ldap_base_dn_group_filter']
            base_dn = ldap_base_dn_group_filter + ',' + ldap_dn
            ldap_group_filter = '(&(cn=*)(memberUid=%s))' % (userId)
            con.search(
                search_base=base_dn,
                search_scope=SUBTREE,
                search_filter=ldap_group_filter,
                attributes=['cn', 'gidNumber']
            )
            results = con.response
            if results:
                # self.logger.debug('LDAP secondary groups: ' + str(results))
                for res in results:
                    sgids.append(res['attributes']['gidNumber'])
        except Exception as e:
            self.logger.exception("Failed to fetch groups from ldap: " + str(e))
        if not sgids:
            # Fallback, try to get from local system
            sgids = self.get_groups(userId)
        return sgids

    def __get_ldap_user_info(self, login, bind=None):
        '''
        Get user info from ldap, optionally bind user for auth

        :param login: user identifier
        :type login: str
        :param bind: password if bind is needed, if None, no binding is done
        :type bind: str
        :return: dict User information
        '''
        user = None
        con = None
        try:
            ldap_host = self.cfg['ldap_host']
            ldap_port = self.cfg['ldap_port']
            s = Server(
                host=ldap_host,
                port=int(ldap_port),
                use_ssl=self.cfg['ldap']['ssl'],
                get_info='ALL'
            )
            con = None
            if 'ldap_admin_dn' in self.cfg and self.cfg['ldap_admin_dn']:
                con = Connection(
                    s,
                    user=self.cfg['ldap_admin_dn'],
                    password=self.cfg['ldap_admin_password']
                )
            else:
                con = Connection(s)
            if self.cfg['ldap']['ssl']:
                con.start_tls()
        except Exception as err:
            self.logger.error(str(err))
            return None

        ldap_dn = self.cfg['ldap_dn']
        ldap_base_dn_filter = 'ou=People'
        if 'ldap_base_dn_filter' in self.cfg and self.cfg['ldap_base_dn_filter']:
            ldap_base_dn_filter = self.cfg['ldap_base_dn_filter']
        base_dn = ldap_base_dn_filter + ',' + ldap_dn

        # filter = "(&""(|(uid=" + login + ")(mail=" + login + ")))"
        ldap_filter = "(&""(|"
        for elt in self.cfg['ldap']['ids']:
            ldap_filter += "(" + elt + "=" + login + ")"
        ldap_filter += "))"

        try:
            if not con.bind():
                self.logger.error('LDAP simple anon bind failed')
                return None

            homeDirectoryField = 'homeDirectory'
            if 'ldap' in self.cfg and 'fields' in self.cfg['ldap'] and 'homeDirectory' in self.cfg['ldap']['fields']:
                homeDirectoryField = self.cfg['ldap']['fields']['homeDirectory']

            attrs = ['mail', 'uid', 'uidNumber', 'gidNumber', homeDirectoryField]
            con.search(
                search_base=base_dn,
                search_scope=SUBTREE,
                search_filter=ldap_filter,
                attributes=attrs
            )
            results = con.response
            user_dn = None
            if results:
                ldapMail = None
                userId = None
                uidNumber = None
                gidNumber = None
                homeDirectory = None
                # user_dn = None
                for res in results:
                    dn = res['dn']
                    user_dn = str(dn)
                    entry = res['raw_attributes']
                    if 'uid' not in entry:
                        self.logger.error('Uid not set for user ' + user)
                    userId = entry['uid'][0]
                    uidNumber = entry['uidNumber'][0]
                    gidNumber = entry['gidNumber'][0]
                    homeDirectory = entry[homeDirectoryField][0]
                    if 'mail' in entry and entry['mail']:
                        ldapMail = entry['mail'][0]

                sgids = self.__get_ldap_groups(con, userId)

                if bind is not None:
                    if not user_dn or not bind:
                        return None
                    # Check credentials
                    con = Connection(s, user=user_dn, password=bind)
                    if not con.bind():
                        self.logger.info('User binding failed')
                        return None
                    con.unbind()

                user = {
                      'id': userId,
                      'uidNumber': uidNumber,
                      'gidNumber': gidNumber,
                      'sgids': sgids,
                      'email': ldapMail,
                      'homeDirectory': homeDirectory
                    }

        except Exception as err:
            self.logger.exception(str(err))
        return user

    def get_user(self, login):
        '''
        Get user information

        Returns a user dict:
                 {
                  'id' : userId,
                  'uidNumber': systemUserid,
                  'gidNumber': systemGroupid,
                  'sgids': list of user secondary group ids,
                  'email': userEmail,
                  'homeDirectory': userHomeDirectory
                  }
        '''
        return self.__get_ldap_user_info(login)

    def bind_credentials(self, login, password):
        '''
        Check user credentials and return user info

        Returns a user dict:
                 {
                  'id' : userId,
                  'uidNumber': systemUserid,
                  'gidNumber': systemGroupid,
                  'sgids': list of user secondary group ids,
                  'email': userEmail,
                  'homeDirectory': userHomeDirectory
                  }
        '''
        return self.__get_ldap_user_info(login, bind=password)

    def bind_api(self, login, apikey):
        '''
        Check api key and return user info (same than bind_credentials)
        '''
        user_in_db = self.users_handler.find_one({'id': login})
        if user_in_db is None:
            return None
        if apikey != user_in_db['credentials']['apikey']:
            return None
        try:
            user = {
                 'id': login,
                 'uidNumber': user_in_db['uid'],
                 'gidNumber': user_in_db['gid'],
                 'sgids': user_in_db['sgids'],
                 'email': user_in_db['email'],
                 'homeDirectory': user_in_db['homeDirectory']
               }
        except Exception:
            return None

        return user

    def get_volumes(self, user, requested_volumes, root_access=False):
        '''
        Returns a list of container volumes to mount, with acls, according to user requested volumes.
        Returned volumes should set real path to requested volumes, possibliy changed requested acl.

        :param user: User returned by bind_credentials or bind_api
        :type user: dict
        :param requested_volumes: list of volumes user expects to be mounted in container
        :type requested_volumes: list
        :param root_access: user request root access to the container
        :type root_access: bool
        :return: list of volumes to mount

        Volumes path are system specific and this method must be implemented according to each system.

        If 'mount' is None, then mount path is the same than original directory.

        requested_volumes looks like:

        volumes: [
            { 'name': 'home',
              'acl': 'rw'
            },
            { 'name': 'omaha',
              'acl': 'rw',
              'path': '/omaha/$USERID',
              'mount': '/omaha/$USERID'
            },
            { 'name': 'db',
              'acl': 'ro',
              'path': '/db'
            },
        ]

        Return volumes:

            volumes: [
                { 'name': 'home',
                  'acl': 'rw',
                  'path': '/home/mygroup/myuserid',
                  'mount': '/home/myuserid'
                },
                { 'name': 'omaha',
                  'acl': 'ro',
                  'path': '/mynfsshare/myuserid'
                  'mount': None
                },
                { 'name': 'db',
                  'acl': 'ro',
                  'path': '/db',
                  'mount': None
                },
            ]


        '''
        volumes = []
        config_volumes = {}
        if len(requested_volumes) > 0:
            for vol in self.cfg['volumes']:
                config_volumes[vol['name']] = vol

        for req in requested_volumes:
            if req['name'] == 'go-docker':
                continue
            if req['name'] == 'god-ftp':
                continue
            if req['name'] == 'home':
                req['path'] = user['homeDirectory']
                req['mount'] = '/mnt/home'
            else:
                if req['name'] not in config_volumes:
                    continue

                req['path'] = config_volumes[req['name']]['path'].replace('$USERID', user['id'])
                if 'mount' not in config_volumes[req['name']] or config_volumes[req['name']]['mount'] is None or config_volumes[req['name']]['mount'] == '':
                    req['mount'] = req['path']
                else:
                    req['mount'] = config_volumes[req['name']]['mount'].replace('$USERID', user['id'])
            if 'acl' not in req:
                req['acl'] = 'ro'
            if config_volumes[req['name']]['acl'] == 'ro':
                req['acl'] = 'ro'
            if root_access:
                req['acl'] = 'ro'
            if 'volumes_check' in self.cfg and self.cfg['volumes_check'] and not os.path.exists(req['path']):
                self.logger.error('Volume path ' + str(req['path']) + ' does not exists')
                continue
            volumes.append(req)

        return volumes
